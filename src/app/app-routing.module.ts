import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'create-recipe',
    pathMatch: 'full'
  },
  {
    path: 'create-recipe',
    loadChildren: () => import('./create-recipe/create-recipe.module').then( m => m.CreateRecipePageModule)
  },
  {
    path: 'update-todo',
    loadChildren: () => import('./update-recipe/update-recipe.module').then( m => m.UpdateRecipePageModule)
  },
  {
    path: 'todo-list',
    loadChildren: () => import('./recipe-list/recipe-list.module').then( m => m.RecipeListPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
