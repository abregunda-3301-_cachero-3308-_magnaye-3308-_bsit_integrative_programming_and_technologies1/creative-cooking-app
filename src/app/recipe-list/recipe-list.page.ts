import { Component, OnInit } from '@angular/core';
import { CrudService } from '../services/crud.service';

export class TODO {
  $key: string;
  title: string;
  description: string;
}

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.page.html',
  styleUrls: ['./recipe-list.page.scss'],
})

export class RecipeListPage implements OnInit {

  tasks: TODO[];

  constructor(private crudService: CrudService) { }

  ngOnInit() {
    this.crudService.getTasks().subscribe((res) => {
      this.tasks = res.map((t) => ({
          id: t.payload.doc.id,
          ...t.payload.doc.data() as TODO
        }));
    });
  }

  todoList() {
    this.crudService.getTasks()
    .subscribe((data) => {
      console.log(data);
    });
  }

  remove(id: string) {
    console.log(id);
    if (window.confirm('Are you sure?')) {
      this.crudService.delete(id);
    }
  }

}
