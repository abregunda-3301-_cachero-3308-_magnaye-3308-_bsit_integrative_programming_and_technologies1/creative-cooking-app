
export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyBE2geB-Z4uFhOavDOSF6EQtD0d9X4yvGw',
    authDomain: 'creative-cooking-app.firebaseapp.com',
    projectId: 'creative-cooking-app',
    storageBucket: 'creative-cooking-app.appspot.com',
    messagingSenderId: '306652475667',
    appId: '1:306652475667:web:acc4c58097a8d35659aae6',
    measurementId: 'G-1F8LPJ6R8R'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
